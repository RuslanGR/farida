from django.urls import re_path

from .views import *


urlpatterns = [
    re_path(r'^comments/$', CommentList.as_view()),
    re_path(r'^comments/(?P<pk>\d+)/$', DoctorComments.as_view()),

    re_path(r'^patients/$', PatientList.as_view()),
    re_path(r'^patients/(?P<pk>\d+)/$', PatientDetail.as_view()),

    re_path(r'^etiological_factors/$', EtiologicalFactorList.as_view()),
    re_path(r'^etiological_factors/(?P<pk>\d+)/$', EtiologicalFactorDetail.as_view()),

    re_path(r'^illnesses/$', IllnessList.as_view()),
    re_path(r'^illnesses/(?P<pk>\d+)/$', IllnessDetail.as_view()),

    re_path(r'^doctors/$', DoctorList.as_view()),
    re_path(r'^doctors/(?P<pk>\d+)/$', DoctorDetail.as_view()),
]
