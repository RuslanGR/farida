from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Comment(models.Model):
    text = models.TextField()
    created_at = models.DateTimeField(auto_now=True)
    sender = models.ForeignKey('Patient', on_delete=models.CASCADE)
    doctor = models.ForeignKey('Doctor', on_delete=models.CASCADE)


class Doctor(models.Model):
    user = models.OneToOneField('auth.User', on_delete=models.CASCADE)
    phone_number = models.CharField(max_length=20)


class Illness(models.Model):
    name = models.CharField(max_length=120)
    etiological_factor = models.ForeignKey('EtiologicalFactor', on_delete=models.CASCADE)


class Patient(models.Model):
    user = models.OneToOneField('auth.User', on_delete=models.CASCADE)
    illnesses = models.ManyToManyField('Illness')


class EtiologicalFactor(models.Model):
    name = models.CharField(max_length=120)
