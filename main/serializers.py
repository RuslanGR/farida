from rest_framework.serializers import ModelSerializer
from django.contrib.auth.models import User

from .models import (
    Comment,
    Doctor,
    Patient,
    Illness,
    EtiologicalFactor,
)


class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username']


class PatientSerializer(ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = Patient
        fields = ['id', 'user', 'illnesses']


class DoctorSerializer(ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = Doctor
        fields = ['id', 'user', 'phone_number']


class CommentSerializer(ModelSerializer):
    sender = PatientSerializer()

    class Meta:
        model = Comment
        fields = ['id', 'text', 'sender', 'doctor']


class EtiologicalFactorSerializer(ModelSerializer):
    class Meta:
        model = EtiologicalFactor
        fields = '__all__'


class IllnessSerializer(ModelSerializer):
    etiological_factor = EtiologicalFactorSerializer()
    class Meta:
        model = Illness
        fields = '__all__'
