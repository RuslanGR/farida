import string
from random import randint, choice

from django.db.models import Q
from django.shortcuts import HttpResponse
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.permissions import AllowAny
from loremipsum import get_sentence as base_sentence
from rest_framework.views import APIView
from rest_framework.response import Response

from .models import (
    Comment,
    Doctor,
    Patient,
    Illness,
    EtiologicalFactor,
)
from .serializers import *


def generator(size=8, chars=string.ascii_letters+string.digits):
    return ''.join([choice(chars) for _ in range(size)])


def get_sentence():
    return base_sentence(start_with_lorem=False)\
        .replace("b'", "").replace("'", "")


def add(request):

    users = []
    for _ in range(1000):
        user = User(
            username=generator(),
            email=generator(6)+'@gmail.com',
            password=generator(),
        )
        users.append(user)
    User.objects.bulk_create(users)

    # Etiological Factor
    efs = []
    for _ in range(80):
        ef = EtiologicalFactor(
            name=get_sentence()
        )
        efs.append(ef)
    EtiologicalFactor.objects.bulk_create(efs)

    # Illnesses
    illnesses = []
    for _ in range(3000):
        illness = Illness(
            name=get_sentence(),
            etiological_factor=choice(efs)
        )
        illnesses.append(illness)
    Illness.objects.bulk_create(illnesses, batch_size=500)

    all_illnesses = Illness.objects.all()
    base_users = list(User.objects.filter(Q(doctor__isnull=True) & Q(patient__isnull=True)))

    # Doctors
    doctors = []
    for _, u in zip(range(20), base_users[:20]):
        doctor = Doctor(
            user=u,
            phone_number='+7'+generator(size=9, chars=string.digits)
        )
        doctors.append(doctor)
    Doctor.objects.bulk_create(doctors)

    clean_user = list(User.objects.filter(Q(doctor__isnull=True) | Q(patient__isnull=True)))

    # Patients
    for _, u in zip(range(900), clean_user[:900]):
        patient = Patient.objects.create(
            user=u
        )
        illnesses_ = [choice(all_illnesses) for _ in range(randint(4, 9))]
        patient.illnesses.set(illnesses_)
        patient.save()
    all_patients = Patient.objects.all()

    # Comment
    comments = []
    for _ in range(6000):
        comment = Comment(
            text=get_sentence(),
            sender=choice(all_patients),
            doctor=choice(doctors)
        )
        comments.append(comment)
    Comment.objects.bulk_create(comments, batch_size=500)

    return HttpResponse('Хорош')


class CommentList(ListCreateAPIView):
    queryset = Comment.objects.all()
    permission_classes = [AllowAny]
    serializer_class = CommentSerializer


class CommentDetail(RetrieveUpdateDestroyAPIView):
    queryset = Comment.objects.all()
    permission_classes = [AllowAny]
    serializer_class = CommentSerializer


class DoctorComments(APIView):

    def get(self, _, pk):
        doctor = Doctor.objects.get(pk=pk)
        comments = doctor.comment_set.all()
        serialized_comments = CommentSerializer(comments, many=True).data[:randint(13, 20)]
        return Response({'comments': serialized_comments})


class PatientList(ListCreateAPIView):
    queryset = Patient.objects.all()
    permission_classes = [AllowAny]
    serializer_class = PatientSerializer


class PatientDetail(RetrieveUpdateDestroyAPIView):
    queryset = Patient.objects.all()
    permission_classes = [AllowAny]
    serializer_class = PatientSerializer


class EtiologicalFactorList(ListCreateAPIView):
    queryset = EtiologicalFactor.objects.all()
    permission_classes = [AllowAny]
    serializer_class = EtiologicalFactorSerializer


class EtiologicalFactorDetail(RetrieveUpdateDestroyAPIView):
    queryset = EtiologicalFactor.objects.all()
    permission_classes = [AllowAny]
    serializer_class = EtiologicalFactorSerializer


class IllnessList(ListCreateAPIView):
    queryset = Illness.objects.all()
    permission_classes = [AllowAny]
    serializer_class = IllnessSerializer


class IllnessDetail(RetrieveUpdateDestroyAPIView):
    queryset = Illness.objects.all()
    permission_classes = [AllowAny]
    serializer_class = IllnessSerializer


class DoctorList(ListCreateAPIView):
    queryset = Doctor.objects.all()
    permission_classes = [AllowAny]
    serializer_class = DoctorSerializer


class DoctorDetail(RetrieveUpdateDestroyAPIView):
    queryset = Doctor.objects.all()
    permission_classes = [AllowAny]
    serializer_class = DoctorSerializer
